const express = require("express");
const app = express();
const cors = require("cors");
const routes = require('./routes/api');
require("dotenv").config({ path: "./config.env" });
const port = process.env.PORT || 5000;
app.use(cors());
app.use(express.json());

app.use('/api', routes);

app.listen(port, () => {
    // perform a database connection when server starts
   
    console.log(`Server is running on port: ${port}`);
  });