const express = require("express");
const routes = express.Router();
const NodeCache = require( "node-cache" );
const myCache = new NodeCache();

myCache.set("networks", []);

routes.route("/ping").get(function(req, res) {
    res.json({"status" : "OK", "msg" : "Server up"});
});

routes.route("/register").post(function(req, res) {
    const networkReg = req.body;
    registerNetwork(networkReg);
    
    res.json({"status" : "OK", "msg" : "Network registered"});
});

// network monitor client will register
routes.route("/event").post(function(req, res) {
    const body = req.body;
    recordEvent(body);
    
    res.json({"status" : "OK", "msg" : "Event recorded"});
});

routes.route("/networks").get(function(req, res) {
    res.json(myCache.get("networks"));
});

routes.route("/clear").get(function(req, res) {
    myCache.set("networks", []);
});

const registerNetwork = (networkInfo) => {
    const networkName = networkInfo['networkname'];
    const hosts = networkInfo['hosts'];
    let registrationObj = {};
    let hostsArray = [];
    registrationObj['name'] = networkName;
    console.log(`Registration for network ${networkName}`);
    for (let i = 0;i<hosts.length;i++ ){
        console.log(`Host ${hosts[i]}`);
        let host = {"name" : hosts[i], "lastUpdate" : new Date(), "latency" : 0, "up" : false};
        hostsArray = [...hostsArray, host];

    }
    registrationObj['hosts'] = hostsArray;
    let registry = myCache.get("networks");
    let found = false;
    for (let i = 0;i<registry.length;i++) {
        if (registry[i]['name'] === networkName) {
            registry[i] = registrationObj;
            found = true;
            break;
        }  
    }
    if (!found)
        registry.push(registrationObj);
    myCache.set("networks", registry);
}

/**
 * Log the event object to the cache
 * This function assumes that event will only come from previously registered host
 * Any other host will be ignored.
 * @param  eventObj 
 */
const recordEvent = (eventObj) => {
    const networkName = eventObj['network'];
    const eventType = eventObj['eventtype'];
    const hostname = eventObj['name'];
    let registry = myCache.get("networks");
    console.log(`Recording event for host ${hostname} on network ${networkName}`);
    const regIdx = getNetworkEntry(registry, networkName)
    

    if (regIdx >=0) {
        let network = registry[regIdx];
        let hosts = network.hosts;
        for (let i = 0;i<hosts.length;i++) {
            if (hosts[i].name === hostname) {
                console.log("Found host entry. Updating");
                eventObj['lastUpdate'] = new Date();
                hosts[i] = eventObj;
                break;
            }
        }
        console.log("Saving event to cache");
        myCache.set("networks", registry);
    } else {
        console.log(`WARNING Network ${networkName} not found in registry`);
    }
    console.log(JSON.stringify(eventObj));

}

const getNetworkEntry = (registry, netName) => {
    let index = -1;
    for (let i = 0;i<registry.length;i++) {
        
        if (registry[i].name === netName) {
            index = i;
            break;
        }
    }

    return index;
}
const findNetwork = (registry, netName) => {
    
    for (let i = 0;i<registry.length;i++) {
        
        if (registry[i].name === netName) {
            return registry[i];
        }
    }
}
module.exports = routes;