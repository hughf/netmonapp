import React, {useEffect, useState} from "react";
import axios from 'axios';
import NetMonitor from './components/NetMonitor';
import './App.css';

function App() {
  const [time, setTime] = useState(Date.now());
  const [registry, setRegistry] = useState([]);
  const REFRESH_INTERVAL = 20000;
  const apiURL = "http://localhost:5000/api/networks";
  const clrURL = "http://localhost:5000/api/clear";

  useEffect(() => {
    console.log("Refreshing list");
    const interval = setInterval(() => setTime(Date.now()), REFRESH_INTERVAL);
    axios.get(apiURL)
    .then((res) =>{
      console.log(res);
      setRegistry(res.data);
    })
    .catch((err) => {
      console.log(err);
    });

    return () => {
      clearInterval(interval);
    };
  }, [time]);

  const clearRegistry = () => {
   
    axios.get(clrURL)
    .then((res) => {
      console.log("Clearing network registry");
      setRegistry([]);
    })
    .catch((err) => {
      console.log(err);
    });
  }
  return (
    <div className="App">
      <NetMonitor networks={registry} lastUpdate={time} clrFunc={clearRegistry}/>
    </div>
  );
}

export default App;
