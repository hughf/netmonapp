import React from 'react';
import Host from './Host';
import './Hosts.css';

function Hosts({hosts}) {
console.log(`Number of hosts passed in ${hosts.length}`);
    return (
        <div className="hostsList">
            {hosts.map((host, idx) => {
                return (
                    <Host key={host.name} entry={host} />   
                )
            })}
        </div>
    )
}

export default Hosts;