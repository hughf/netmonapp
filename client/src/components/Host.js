import React from 'react';
import moment from 'moment';
import './Host.css';

function Host({entry}) {

    return (
        <div className="hostParams">
            <div className="hostParam">
                <span className="hostParamCell paramLabel">Hostname</span>
                <span className="hostParamCell paramValue">{entry.name}</span>
            </div>
            <div className="hostParam">
                <span className="hostParamCell paramLabel">Status</span>
                <span className="hostParamCell paramValue">{(entry.up) ? 'up' : 'down'}</span>
            </div>
            <div className="hostParam">
                <span className="hostParamCell paramLabel">Last Update</span>
                <span className="hostParamCell paramValue">{moment(entry.lastUpdate).fromNow()}</span>
            </div>
            <div className="hostParam">
                <span className="hostParamCell paramLabel">Latency</span>
                <span className="hostParamCell paramValue">{entry.latency}</span>
            </div>
        </div>
    )
}
export default Host;