import React from 'react';
import moment from 'moment';
import axios from 'axios';
import Hosts from './Hosts';
import './NetMonitor.css';

function NetMonitor({networks,lastUpdate,clrFunc}) {
    console.log(networks);
    return (
        <>
        <h1>Basic network monitor</h1>
        <h3>Last Updated {moment(lastUpdate).fromNow()}</h3>
        <div className="ctrlRow">
            <button name="clear" onClick={clrFunc}>Clear registry</button>
        </div>
        {networks.map((network, idx) => {
            return (
                <div className="networkDiv" key={network.name}>
                    <div className="netLabel">Network Name:{network.name}</div>
                    <div className="netLabel">Number of hosts: {network.hosts.length}</div>
                    <Hosts hosts={network.hosts} />
                </div>
            )
        })}
        </>
    )
    
}

export default NetMonitor;